module.exports = {
  pathPrefix: `/gatsby`,
  siteMetadata: {
    title: `React Training`
  },
  plugins: [
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [`gatsby-remark-responsive-iframe`]
      }
    },
    `gatsby-plugin-react-helmet`
  ]
};
