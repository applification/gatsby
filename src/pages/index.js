import React from 'react';
import Link from 'gatsby-link';

const IndexPage = () => (
  <div>
    <h1>React &amp; React Native Training</h1>
    <Link to="/react/learn-once">Learn Once Write Anywhere</Link>
    <br />
    <Link to="/react/functional">Functional Programming</Link>
    <br />
    <Link to="/react/raw-api">Using Raw React API</Link>
    <br />
    <Link to="/react/jsx-react">JSX with React</Link>
    <br />
    <Link to="/react/pure-components">Pure Components</Link>
    <br />
    <Link to="/react/super-components">Super Components</Link>
    <br />
    <Link to="/react/proptypes">PropTypes</Link>
    <br />
    <Link to="/react/children">Children</Link>
    <br />
    <Link to="/react/conditional-rendering">Conditional Rendering</Link>
    <br />
    <Link to="/react/selective-rendering">Selective Rendering</Link>
    <br />
    <Link to="/react/styling-react">Styling React</Link>
    <br />
    <Link to="/react/events">Events in React</Link>
    <br />
    <Link to="/react/state">State in React</Link>
    <br />
  </div>
);

export default IndexPage;
