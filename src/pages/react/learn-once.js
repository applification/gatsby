import React from 'react';
import Link from 'gatsby-link';
import Highlight from 'react-highlight';
import '../../../node_modules/highlight.js/styles/tomorrow-night-bright.css';

const LearnOnce = () => {
  return (
    <div>
      <h1>Learn Once, Write Anywhere</h1>
      <p>
        React enables you to learn an approach that can then be applied to a
        variety of different mediums.
      </p>
      <p>
        If you know React, then you can easily learn React Native as 70% is the
        same, the differences are around the output medium (web vs mobile).{' '}
      </p>
      <p>
        A single development team can cover web &amp; mobile which is incredibly
        powerful adn cost saving.
      </p>

      <h4>Read More</h4>
      <ul>
        <li>Desktop Apps with Electron</li>
        <li>CLIE apps</li>
        <li>Sketch</li>
        <li>AR</li>
      </ul>
    </div>
  );
};

export default LearnOnce;
