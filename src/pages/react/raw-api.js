import React from 'react';
import Link from 'gatsby-link';
import Highlight from 'react-highlight';
import '../../../node_modules/highlight.js/styles/tomorrow-night-bright.css';

const IndexPage = () => (
  <div>
    <h1>Using Raw React API</h1>
    <p>Example of rendering using the raw React API:</p>
    <Highlight>
      {`
        React.createElement('div', {
          children: 'Hello World'
        });      
      `}
    </Highlight>
    <p>
      This highlights the fact that with React you are just writing JavaScript,
      we simply add some syntactic sugar on top in the form of JSX to make it
      easier to write and understand.
    </p>
    <iframe
      src="https://codesandbox.io/embed/m5lm3n6l9"
      width="920"
      height="600"
      sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"
    />
    <Link to="/page-2/">Go to page 2</Link>
  </div>
);

export default IndexPage;
