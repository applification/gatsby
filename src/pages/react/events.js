import React from 'react';
import Link from 'gatsby-link';
import Highlight from 'react-highlight';
import '../../../node_modules/highlight.js/styles/tomorrow-night-bright.css';

const events = () => {
  return (
    <div>
      <h1>Events in React</h1>
      <p>In HTML events were inline and used strings</p>
      <Highlight>
        {`
<button
  onmousenter="alert('imma...')"
  onmousleave="alert('just passing through.')"
  onclick="alert('do that thing you do!')"
>Do something</button>   
      `}
      </Highlight>
      <p>In React events are JavaScript not strings!</p>
      <Highlight>
        {`
<button
  type="button"
  onClick={() => alert(1 + " claps!")}>
  👏
</button>
      `}
      </Highlight>

      <p>
        When rendered to the DOM you'll notice the event handler is no more.
        React did the work of making a bad practics a good practice by adding
        our event handler at the top of the document.
      </p>
      <Highlight>
        {`
<div>
    <button type="button">👏</button>
    <i>be the first to clap</i>
</div>
      `}
      </Highlight>

      <hr />
      <h4>Read More</h4>
      <ul>
        <li>
          <a href="https://reactjs.org/docs/handling-events.html">
            Handling Events
          </a>
        </li>
        <li>
          <a href="https://reactjs.org/docs/events.html">
            React SyntheticEvent
          </a>
        </li>
      </ul>
    </div>
  );
};

export default events;
