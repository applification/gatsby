import React from 'react';
import Link from 'gatsby-link';
import Highlight from 'react-highlight';
import '../../../node_modules/highlight.js/styles/tomorrow-night-bright.css';

const ConditionalRendering = () => {
  return (
    <div>
      <h1>Conditional Rendering</h1>
      <p>
        Rendering different components is a common use case in React, for
        example when getting data from an API you often render different UI
        depending on the following states:{' '}
      </p>
      <ul>
        <li>loading</li>
        <li>loaded</li>
        <li>error</li>
      </ul>

      <p>However, you can't use if/else statements in render()</p>
      <Highlight>
        {`
  render() {
    return (
      <div>{/* no if/else */}</div>
    )
  }   
      `}
      </Highlight>
      <p>In those JSX curly-braces, you can only use expressions.</p>
      <h2>Tern or Burn</h2>
      <p>
        Rendering conditionally is done using the{' '}
        <a
          href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_Operator"
          target="_blank"
        >
          conditional ternary operator
        </a>{' '}
        instead.
      </p>
      <Highlight>
        {`
  // just like if/else
  someCondition ? 'shown if truthy' : 'shown if falsy' 
      `}
      </Highlight>

      <h2>Short circuit</h2>
      <p>
        <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Logical_Operators#Short-circuit_evaluation">
          Short-circut evaluation
        </a>{' '}
        also works too
      </p>
      <Highlight>
        {`
  someCondition && 'shown only if truthy' // if
  someCondition || 'shown only if falsy' // unless
      `}
      </Highlight>
      <iframe
        src="https://codesandbox.io/embed/1319l840q3"
        width="920"
        height="600"
        sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"
      />
    </div>
  );
};

export default ConditionalRendering;
