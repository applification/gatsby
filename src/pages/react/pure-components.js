import React from 'react';
import Link from 'gatsby-link';
import Highlight from 'react-highlight';
import '../../../node_modules/highlight.js/styles/tomorrow-night-bright.css';

const PureComponents = () => {
  return (
    <div>
      <h1>React Pure Components</h1>
      <p>
        React is about the creation of simple components that can be tested and
        re-used.{' '}
      </p>
      <p>Components are functions()!!</p>
      <Highlight>
        {`
        function Greeting() {
          return <h1>Hello 🎄</h1>;
        }

        <Greeting />    
      `}
      </Highlight>
      <p>
        If components are functions and functions take arguments, components
        should take arguments; and they do! In React component arguments are
        called props.{' '}
      </p>
      <p>In JSX props are authored like HTML attributes.</p>
      <Highlight>
        {`
          <Greeting name="bulbasaur" />    
      `}
      </Highlight>
      <p>
        To use inside your component function React puts props in a single
        object; and that object is conveniently our first argument of our
        component function.{' '}
      </p>
      <p>
        Once you have the props object, you can interpolate values in with {}.
      </p>
      <Highlight>
        {`
          function Greeting(props) {
            return <h1>Hello {props.name}</h1>;
          }  
      `}
      </Highlight>
      <p>
        The example below creates a Message component that accepts some text and
        displays it. It's simple in nature but illustrates how we may
        componentise our app.
      </p>
      <p>
        NB: Components MUST be capitalised, message would break whilst Message
        works as expected.
      </p>
      <iframe
        src="https://codesandbox.io/embed/p3ovy2wm07"
        width="920"
        height="600"
        sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"
      />
    </div>
  );
};

export default PureComponents;
