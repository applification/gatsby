import React from 'react';
import Link from 'gatsby-link';
import Highlight from 'react-highlight';
import '../../../node_modules/highlight.js/styles/tomorrow-night-bright.css';

const Children = () => {
  return (
    <div>
      <h1>Children</h1>
      <p>All React components get a special prop: children.</p>
      <p>It can be used to merge components together</p>
      <Highlight>
        {`
  render () {
    <div>
      {this.props.children}
    </div>
  }
      `}
      </Highlight>
    </div>
  );
};

export default Children;
