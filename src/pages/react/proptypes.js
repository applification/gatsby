import React from 'react';
import Link from 'gatsby-link';
import Highlight from 'react-highlight';
import '../../../node_modules/highlight.js/styles/tomorrow-night-bright.css';

const PropTypes = () => {
  return (
    <div>
      <h1>PropTypes</h1>
      <p>
        When using components sometimes people make mistakes passing in props
        and things break. To help protect against this you've got PropTypes
        which is a separate NPM package you need to install.{' '}
      </p>
      <p>
        NB: In production builds PropTypes are ignored because they slow things
        down, if you want you can also have a build step that removes all
        PropTypes for further speed improvements.{' '}
      </p>
      <h2>Setting PropTypes with a function</h2>
      <Highlight>
        {`
        import PropTypes from 'prop-types'; 

        function SayHello(props) {
          return (
            <div>
              Hello {props.firstName} {props.lastName}!
            </div>
          )
        }

        SayHello.propTypes = {
          firstName: PropTypes.string.isRequired,
          lastName: PropTypes.string.isRequired,
        } 
      `}
      </Highlight>
      <h2>Setting PropTypes when using ES6 classes</h2>
      <Highlight>
        {`
        import React from 'react';
        import { render } from 'react-dom';
        import PropTypes from 'prop-types'; 
        
        class SayHello extends React.Component {
          static PropTypes = {
            firstName: PropTypes.string.isRequired,
            lastName: PropTypes.string.isRequired
          }
        
          render() {
            const { firstName, lastName } = this.props
            return (
              <div>
                Hello {firstName}, {lastName}
              </div>
            )
          }
        }
        
        render(<SayHello firstName="Dave" lastName="Hudson" />, document.getElementById('root'));        
      `}
      </Highlight>
    </div>
  );
};

export default PropTypes;
