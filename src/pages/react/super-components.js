import React from 'react';
import Link from 'gatsby-link';
import Highlight from 'react-highlight';
import '../../../node_modules/highlight.js/styles/tomorrow-night-bright.css';

const SuperComponents = () => {
  return (
    <div>
      <h1>React Super Components</h1>
      <p>Components can uses classes for special powers.</p>
      <p>
        Unlike function components, class components have an instance—this. This
        gives us lifecycle methods and state APIs for some seriously cool
        interactivity.
      </p>
      <Highlight>
        {`
          class Greeting extends React.Component {
            render() {
              return (
                <h1>Hello {this.props.name}.</h1>
              );
            }
          }   
      `}
      </Highlight>

      <h2>The Component Lifecycle</h2>
      <a href="https://reactjs.org/docs/react-component.html" target="_blank">
        React.Component Docs
      </a>
      <p>
        Each component has several “lifecycle methods” that you can override to
        run code at particular times in the process. Methods prefixed with will
        are called right before something happens, and methods prefixed with did
        are called right after something happens.
      </p>

      <h3>Mounting</h3>
      <p>
        These methods are called when an instance of a component is being
        created and inserted into the DOM:
      </p>
      <ul>
        <li>constructor()</li>
        <li>componentWillMount()</li>
        <li>render()</li>
        <li>componentDidMount()</li>
      </ul>

      <h3>Updating</h3>
      <p>
        An update can be caused by changes to props or state. These methods are
        called when a component is being re-rendered:
      </p>
      <ul>
        <li>componentWillReceiveProps()</li>
        <li>shouldComponentUpdate()</li>
        <li>componentWillUpdate()</li>
        <li>render()</li>
        <li>componentDidUpdate()</li>
      </ul>

      <h3>Unmounting</h3>
      <p>
        This method is called when a component is being removed from the DOM:
      </p>
      <ul>
        <li>componentWillUnmount()</li>
      </ul>

      <h3>Error Handling</h3>
      <p>
        This method is called when there is an error during rendering, in a
        lifecycle method, or in the constructor of any child component.
      </p>
      <ul>
        <li>componentDidCatch()</li>
      </ul>
    </div>
  );
};

export default SuperComponents;
