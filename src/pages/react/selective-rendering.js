import React from 'react';
import Link from 'gatsby-link';
import Highlight from 'react-highlight';
import '../../../node_modules/highlight.js/styles/tomorrow-night-bright.css';

const SelectiveRendering = () => (
  <div>
    <h1>Selective Rendering</h1>
    <p>
      React is intelligent, rather than re-rendering the entire {`<div>`} it
      will ONLY update the {`{time}`} value WITHIN the {`<input/>`} tag. This is
      huge in terms of performance and UX.
    </p>
    <Highlight>
      {`
  const rootElement = document.getElementById('root')
  function tick() {
    const time = new Date().toLocaleTimeString()
    const element = (
      <div>
        It is
        <input value={time} />
        <input value={time} />
      </div>
    )
    ReactDOM.render(element, rootElement)
  }
  tick()
  setInterval(tick, 1000)    
      `}
    </Highlight>
  </div>
);

export default SelectiveRendering;
