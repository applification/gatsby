import React from 'react';
import Link from 'gatsby-link';
import Highlight from 'react-highlight';
import '../../../node_modules/highlight.js/styles/tomorrow-night-bright.css';

const JSXReact = () => (
  <div>
    <h1>JSX with React</h1>
    <p>
      Writing out Javascript React.createElement all the time would be slow and
      cumbersome, which is why JSX was invented to provide an HTML like
      experience.
    </p>
    <p>
      In the example below we create a props object with a class and the text as
      children. We then simply create a div tag and spread the props object into
      the div. In effect this renders as:
    </p>
    <Highlight>
      {`
        <div className="container">Hello World with React</div>
      `}
    </Highlight>

    <p>
      Note: This example uses Create React App and Babel is transpiling the JSX
      to JS behind the scenes for you.
    </p>
    <p>
      You could use unpkg to experiment in plain HTML
      https://unpkg.com/babel-standalone@6.26.0/babel.js{' '}
    </p>

    <iframe
      src="https://codesandbox.io/embed/3v34k1o7p"
      width="920"
      height="600"
      sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"
    />

    <h2>JSX</h2>
    <p>
      As JSX is really JavaScript there are differences to HTML. For example the
      class prop needs to be named className:
    </p>
    <Highlight>
      {`
        // HTML
        <div class="underline" />

        // JSX
        <div className="underline" />      
      `}
    </Highlight>

    <h2>JSX Interpolation</h2>
    <p>JSX can contain JavaScript using interpolation</p>
    <Highlight>
      {`
        // JSX with interpolation
        const underline = "_underline"
        <div className={underline} />

        // JSX interpolation spread props
        const props = {
          className: 'container',
          children: 'Hello world'
        }
        <div {...props} />

        // JSX Spread Props with Overide
        const props = {
          className: 'container',
          children: 'Hello world'
        }
        <div {...props} className="MY_contianer" />     
      `}
    </Highlight>
  </div>
);

export default JSXReact;
