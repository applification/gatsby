import React from 'react';
import Link from 'gatsby-link';
import Highlight from 'react-highlight';
import '../../../node_modules/highlight.js/styles/tomorrow-night-bright.css';

const State = () => {
  return (
    <div>
      <h1>State in React</h1>
      <p>
        One of the special powers of class components is their ability to hold
        state.
      </p>
      <p>
        With state, you get interactivity. No state, no interactivity. So
        component state isn't just a feature, it's the feature.
      </p>

      <h2>State in constructor()</h2>
      <Highlight>
        {`
  constructor() {
    super();
    this.state = { trumpIsMad: true }
  }    
      `}
      </Highlight>

      <h2>State with ES6 syntax</h2>
      <Highlight>
        {`
  state = { trumpIsMad: true }
      `}
      </Highlight>

      <h2>Setting State</h2>
      <Highlight>
        {`
  this.setState({ someProp: newState })  
      `}
      </Highlight>
      <p>
        This is a special power our compoenents get by extending
        React.Component. When called, setState() updates the local state object
        and re-renders the component.
      </p>
      <p>If you want to set state dynamically, use a function</p>
      <Highlight>
        {`
  this.setState((previousState) =>
    ({ clapCount: previousState.clapCount + 1 })
  )
      `}
      </Highlight>
      <p>
        <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions">
          Arrow functions
        </a>{' '}
        have implicit returns but if the return type is an object, you must wrap
        it in parentheses.
      </p>
      <p>
        While setState does update state—eventually—it does so asyncronously and
        in batches. Because of that implementation detail, the state that your
        function recieves might not be the current state.
      </p>
      <iframe
        src="https://codesandbox.io/embed/r7pzm1p7kn"
        width="920"
        height="600"
        sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"
      />

      <h2>State Management</h2>
      <ul>
        <li>Redux</li>
        <li>Mobx</li>
        <li>Apollo</li>
      </ul>
    </div>
  );
};

export default State;
