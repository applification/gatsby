import React from 'react';
import Link from 'gatsby-link';
import Highlight from 'react-highlight';
import '../../../node_modules/highlight.js/styles/tomorrow-night-bright.css';

const Functional = () => {
  return (
    <div>
      <h1>Functional Programming</h1>
      <ul>
        <li>Pure Functions</li>
        <li>Immutable Data</li>
        <li>Storybook</li>
      </ul>
    </div>
  );
};

export default Functional;
