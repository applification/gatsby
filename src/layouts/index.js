import React from 'react';
import PropTypes from 'prop-types';
import Link from 'gatsby-link';
import Helmet from 'react-helmet';

import logo from '../imgs/Applification-Logo-Blue.png';
import logoreact from '../imgs/React.png';
import './index.css';

const Header = () => (
  <div
    style={{
      background: 'white',
      marginBottom: '1.45rem'
    }}
  >
    <div
      style={{
        margin: '0 auto',
        maxWidth: 960,
        padding: '1.45rem 1.0875rem'
      }}
    >
      <h1 style={{ margin: 0 }}>
        <Link
          to="/"
          style={{
            color: '#777',
            textDecoration: 'none'
          }}
        >
          <img src={logo} alt="Applification" height="100" />
          <img src={logoreact} alt="Applification" height="100" />
        </Link>
      </h1>
    </div>
  </div>
);

const TemplateWrapper = ({ children }) => (
  <div>
    <Helmet
      title="React Training"
      meta={[
        { name: 'description', content: 'React & React Native Training' },
        { name: 'keywords', content: 'react, react native, training' }
      ]}
    />
    <Header />
    <div
      style={{
        margin: '0 auto',
        maxWidth: 960,
        padding: '0px 1.0875rem 1.45rem',
        paddingTop: 0
      }}
    >
      {children()}
    </div>
  </div>
);

TemplateWrapper.propTypes = {
  children: PropTypes.func
};

export default TemplateWrapper;
